const {
  listTodoList,
  detailTodoList,
  createTodoList,
  modifyTodoList,
  deleteTodoList,
} = require("../services/todoList.service");
const { check, validationResult } = require("express-validator");

function todoList(app) {
  app.get("/todoList", async (req, res) => {
    const errors = validationResult(req);
    const todoList = await listTodoList(req.query.page, req.query.size);
    res.send(todoList);
  });

  app.get("/todoList/:id", [check("id").isAlphanumeric()], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.send({ errors: errors.array() });
    }
    const todoList = await detailTodoList(req.params.id);
    res.send(todoList);
  });
  app.post(
    "/todoList",
    [check("date").isDate(), check("finished").isBoolean()],
    async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.send({ errors: errors.array() });
      }
      await createTodoList(req.body);
      res.send({ message: "added successfully" });
    }
  );
  app.put(
    "/todoList",
    [
      check("id").isAlphanumeric(),
      check("date").isDate(),
      check("detail").isAlphanumeric(),
      check("finished").isBoolean(),
    ],
    async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.send({ errors: errors.array() });
      }
      await modifyTodoList(req.body);
      res.send({ message: "modified successfully" });
    }
  );
  app.delete(
    "/todoList/:id",
    [check("id").isAlphanumeric()],
    async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.send({ errors: errors.array() });
      }
      await deleteTodoList(req.params.id);
      res.send({ message: "deleted successfully" });
    }
  );
}
module.exports = {
  todoList,
};
