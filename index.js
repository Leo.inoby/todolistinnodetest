const express = require("express");
const { todoList } = require("./controller/todoList");
var bodyParser = require("body-parser");
const app = express();

app.use(bodyParser.json());

todoList(app);

app.listen(3000, function () {});
