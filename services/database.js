const { MongoClient } = require("mongodb");
const url = "mongodb://localhost";
const client = new MongoClient(url);

async function getDb() {
  await client.connect();
  const db = client.db("todoList");
  return db;
}
async function getCollection(collectionName) {
  const db = await getDb();
  const collection = await db.collection(collectionName);
  return collection;
}
async function getTodoList() {
  const db = await getCollection("todoList");
  return db;
}

module.exports = {
  getDb,
  getCollection,
  getTodoList,
};
