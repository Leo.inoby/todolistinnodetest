const { ObjectId } = require("mongodb");

const { getTodoList } = require("./database");

async function listTodoList(page = 1, size = 20) {
  const todoListsCollection = await getTodoList();
  const conditions = {};
  const todoList = await todoListsCollection
    .find()
    .sort({ _id: 1 })
    .skip((parseInt(page) - 1) * parseInt(size))
    .limit(parseInt(size))
    .toArray();
  return todoList;
}
async function detailTodoList(id) {
  const todoListsCollection = await getTodoList();
  const conditions = {
    _id: new ObjectId(id),
  };
  const todoList = await todoListsCollection.findOne(conditions);
  return todoList;
}
async function createTodoList(todoList) {
  const todoListsCollection = await getTodoList();
  const newTodoList = {
    date: todoList.date,
    detail: todoList.detail,
    finished: todoList.finished,
  };
  await todoListsCollection.insertOne(newTodoList);
}
async function modifyTodoList(todoList) {
  const todoListsCollection = await getTodoList();
  const conditions = {
    _id: new ObjectId(todoList.id),
  };
  const update = {
    $set: {
      date: todoList.date,
      detail: todoList.detail,
      finished: todoList.finished,
    },
  };
  await todoListsCollection.updateOne(conditions, update);
}
async function deleteTodoList(id) {
  const todoListsCollection = await getTodoList();
  const conditions = {
    _id: new ObjectId(id),
  };
  await todoListsCollection.deleteOne(conditions);
}

module.exports = {
  listTodoList,
  detailTodoList,
  createTodoList,
  modifyTodoList,
  deleteTodoList,
};
